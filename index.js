const commentsContainer = document.querySelector(".comments");
const container = document.querySelector(".container");

let comments;
let currentUser;

const createComment = (
  id,
  image,
  username,
  createdAt,
  content,
  score,
  replyingTo,
  isCurrentUser
) => {
  const comment = document.createElement("div");
  comment.setAttribute("id", id);
  comment.classList.add("comment");

  comment.innerHTML = `
          <!--Top profile content-->
          <div class="comment__profile">
            <img
              class="comment__profile__picture"
              src="${image.png}"
            />
            <h3 class="comment__profile__username">${
              isCurrentUser
                ? `${username} <span class="comment__profile__username__indicador">you</span>`
                : username
            }</h3>
            <p class="comment__profile__published">${createdAt}</p>
          </div>
          <!-- Comment text-->
          <p
            class="comment__content"
            >${
              replyingTo
                ? `<span class="comment__content__replyTo">@${replyingTo} </span>`
                : ""
            }${content}</p>

          <!--Comment score-->
          <div class="comment__score">
            <button type="button" class="comment__score__button" onclick="vote(${id}, 'up')">
              <img class="comment__score__button__icon" src="./images/icon-plus.svg" alt="plus" />
            </button>
            <p class="comment__score__label">${score}</p>
            <button class="comment__score__button" onclick="vote(${id}, 'down')">
              <img class="comment__score__button__icon" src="./images/icon-minus.svg" alt="minus" />
            </button>
          </div>
          <!--Comment actions-->
          <div class="comment__actions">
          ${
            isCurrentUser
              ? `<button type="button" class="comment__actions__button comment__actions__button--delete">
              <img class="comment__actions__button__icon" src="./images/icon-delete.svg" alt="delete"/>
              <p
                class="comment__actions__label"
              >
                Delete
              </p>
            </button>
            <button class="comment__actions__button" onclick="edit(${id})">
              <img class="comment__actions__button__icon" src="./images/icon-edit.svg" alt="edit" />
              <p class="comment__actions__label">Edit</p>
            </button>`
              : `<button class="comment__actions__button" onclick="reply(${id})">
                <img class="comment__actions__button__icon" src="./images/icon-reply.svg" alt="reply" />
                <p class="comment__actions__label">Reply</p>
              </button>`
          }
          </div>
        `;

  return comment;
};

const loadData = async () => {
  const response = await fetch("data.json");
  const data = await response.json();
  return data;
};

const createTextbox = (image) => {
  const textbox = document.createElement("div");
  textbox.classList.add("textbox");
  textbox.innerHTML = `      
        <textarea
          class="textbox__textarea"
          placeholder="Add a comment..."
        ></textarea>
        <img
          class="textbox__profile__image"
          src="${image}"
        />
        <button class="textbox__button" onclick="send()">Send</button>
      `;
  return textbox;
};

const createComments = (parentDiv, comments, currentUser) => {
  comments.forEach((element) => {
    const comment = createComment(
      element.id,
      element.user.image,
      element.user.username,
      element.createdAt,
      element.content,
      element.score,
      element.replyingTo,
      element.user.username === currentUser?.username // Check if is current user
    );
    // Add Comment/reply to parent div (comments or replies container)
    parentDiv.appendChild(comment);

    // Create replies container
    const replies = document.createElement("div");
    replies.classList.add("replies");
    replies.setAttribute("id", `replies-${element.user.username}`);
    comment.parentNode.insertBefore(replies, comment.nextSibling);

    if (element.replies?.length > 0) {
      // Recursion, call function again but attach to replies container
      createComments(replies, element.replies, currentUser);
    }
  });
};

const vote = (id, type) => {
  const score = document.getElementById(id);
  const label = score.querySelector(".comment__score__label").innerHTML;
  score.querySelector(".comment__score__label").innerHTML =
    type === "up"
      ? Number(label) + 1
      : Number(label) === 0
      ? 0
      : Number(label) - 1;
};

const reply = (id) => {
  // Get replyTo info
  const comment = document.getElementById(id);

  const replyBox = createTextbox(currentUser.image.png);
  comment.parentNode.insertBefore(replyBox, comment.nextSibling);

  // Focus on the textarea and add replyto username
  const textarea = replyBox.querySelector(".textbox__textarea");
  textarea.focus();

  // Update button label
  const buttonLabel = replyBox.querySelector(".textbox__button");
  buttonLabel.innerHTML = "Reply";
  buttonLabel.onclick = () => {
    submitReply(id);
  };
};

const edit = (id) => {
  const comment = document.getElementById(id);
  const content = comment.querySelector(".comment__content");

  // Create edit textarea
  const textarea = document.createElement("textarea");
  textarea.classList.add("textbox__textarea");

  const reply = comments.find((x) => x.id === id)
    ? comments.find((x) => x.id === id)
    : comments
        .find((x) => x.replies.find((y) => y.id === id))
        .replies.find((x) => x.id === id);

  textarea.value = reply.content;
  textarea.focus();

  content.parentNode.insertBefore(textarea, content.nextSibling);

  const actionsButtons = comment.querySelectorAll(".comment__actions__button");
  actionsButtons.forEach((button) => (button.style.display = "none"));

  const updateButton = document.createElement("button");
  updateButton.classList.add("textbox__button");
  updateButton.innerHTML = "Update";
  updateButton.onclick = (e) => {
    update(e, id, reply.replyingTo);
  };

  const actions = comment.querySelector(".comment__actions");
  actions.appendChild(updateButton);

  content.style.display = "none";
};

const update = (e, id, replyTo) => {
  const comment = document.getElementById(id);
  const content = comment.querySelector(".comment__content");
  const textareaContent = comment.querySelector(".textbox__textarea");

  const actionsButtons = comment.querySelectorAll(".comment__actions__button");
  actionsButtons.forEach((button) => (button.style.display = "flex"));

  content.innerHTML =
    `<span class="comment__content__replyTo">@${replyTo} </span>` +
    textareaContent.value;

  textareaContent.style.display = "none";
  content.style.display = "block";

  // Hide update button after press
  e.target.style.display = "none";
};

const submitReply = (id) => {
  const comment = document.getElementById(id);
  const replyTo = comment.querySelector(
    ".comment__profile__username"
  ).innerHTML;

  const textbox = comment.nextElementSibling;
  console.log(textbox);
  const textareaContent = textbox.querySelector(".textbox__textarea");
  console.log(textareaContent);

  const reply = {
    id: Math.floor(Math.random() * 100),
    content: textareaContent.value,
    createdAt: new Date(),
    score: 0,
    replyingTo: replyTo,
    user: {
      image: {
        png: currentUser.image.png,
        webp: currentUser.image.webp,
      },
      username: currentUser.username,
    },
  };

  comments.find((x) => x.id === id).replies.push(reply);

  document
    .getElementById("replies-" + replyTo)
    .appendChild(
      createComment(
        reply.id,
        reply.user.image,
        reply.user.username,
        reply.createdAt.toLocaleDateString(),
        reply.content,
        reply.score,
        reply.replyingTo,
        true
      )
    );

  textbox.style.display = "none";
};

const send = () => {
  const textbox = document.querySelectorAll(".textbox:nth-last-of-type(1)");
  const content = textbox[0].querySelector(".textbox__textarea");

  const comment = {
    id: Math.floor(Math.random() * 100),
    content: content.value,
    createdAt: new Date(),
    score: 0,
    user: {
      image: {
        png: currentUser.image.png,
        webp: currentUser.image.webp,
      },
      username: currentUser.username,
    },
    replies: [],
  };

  commentsContainer.appendChild(
    createComment(
      comment.id,
      comment.user.image,
      comment.user.username,
      comment.createdAt.toLocaleDateString(),
      comment.content,
      comment.score,
      null,
      true
    )
  );
  // Reset textarea
  content.value = "";
};

const loadComments = async () => {
  const data = await loadData();
  comments = data.comments;
  currentUser = data.currentUser;
  createComments(commentsContainer, comments, currentUser);
  container.appendChild(createTextbox(currentUser.image.png));
};

loadComments();
