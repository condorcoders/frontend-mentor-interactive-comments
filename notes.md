## El desafio

Los usuarios deben poder:

- Ver un layout responsive
- ver los estilos hover
- Crear, leer, editar y borrar comentarios
- votar por los comentarios

## Comportamiento

- Comentarios deben de estar ordenados por su calificacion y las respuestas por tiempo de publicacion
- Nuevas respuestas se colocan al final de las respuestas de un comentario
- Un modal debe aparecer cuando se intenta borrar un comentario o respuesta
- Para agregar un comentario o respuesta se utiliza el `currentUser` en `data.json`
- Solo se puede borrar o editar tu propio comentario o respuesta (`currentUser`)
- No puedes responder tu propio comentario

## Bonus:

- Usar localstorage para guardar comentarios
- Calcular el tiempo dinamicamente desde que se publico el comentario
